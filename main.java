import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;
class Main {
  public static void main(String[] args) {
    double pd = 1.0/3.0; //probab of defect
    double pnd =2.0/3.0;  //the contrary
    //probab that the 1st defect occurs at the 5th trial
    double p = Math.pow(pnd, 4)*pd;
    p = Math.round(1000.0*p)/1000.0;
    System.out.println(p);
  }
}
